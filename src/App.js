import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import './App.css';

//components
import TopScreen from './components/TopScreen';
import CreateArticle from './components/CreateArticleScreen';
import ArticleDetail from './components/ArticleDetailScreen';
import EditArticle from './components/EditArticleScreen';

//css
import './App.css';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<TopScreen/>}/>
        <Route exact path="/:postId" element={<ArticleDetail/>}/>
        <Route exact path="/:postId/edit" element={<EditArticle/>}/>
        <Route exact path="/new" element={<CreateArticle/>}/>
      </Routes>
    </Router>
  );
}

export default App;
