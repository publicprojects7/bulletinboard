import {useEffect, useState} from 'react';
import {Container, Form, Button, Row} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom';

export default function ArticleDetail(){

	const [title, setTitle] = useState('');
	const [body, setBody] = useState('');
	const [dateCreated, setDateCreated] = useState('');

	const {postId} = useParams();

	useEffect(() => {

		fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
		.then(res => res.json())
		.then(data => {

			setTitle(data.title);
			setBody(data.body);
		})
	}, []);

	return (
		<Container className="p-3">
			<Button as={Link} to={`/`}>HOME</Button>
			<Row className="border m-3 p-3">
				<h5>Article Title: {title}</h5>
				<p>Content: {body}</p>
			</Row>
		</Container>
	)
}