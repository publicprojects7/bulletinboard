import {useEffect, useState} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CreateArticle(){

	const [article, setArticle] = useState('');

	return (
		<Container className="p-3">
			<Form>
			    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
			        <Form.Label>Title</Form.Label>
			        <Form.Control type="text" placeholder="Article Title" />
			    </Form.Group>
			    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
			        <Form.Label>Content</Form.Label>
			        <Form.Control as="textarea" rows={3} />
			    </Form.Group>
			    <Button type="submit">Post</Button>
			    <Button as={Link} to={`/`} className="ms-3 btn-danger">Cancel</Button>
			</Form>
		</Container>
	)
}