import {Row} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ArticleCards({articleProp}){

	const {id, title, body, dateCreated} = articleProp;

	return(
		<>
			<Row className="border m-3 p-3">
				<h4><Link to={`/${id}`}>Article Title: {title}</Link></h4>
				<p>Content: {body}</p>
				<p>{dateCreated} | <Link to={`/${id}/edit`}>Edit</Link> | <span>Delete</span> </p>
			</Row>
		</>
	)
}