import {useEffect, useState} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom';

export default function EditArticle(){

	const [title, setTitle] = useState('');
	const [body, setBody] = useState('');
	const [dateCreated, setDateCreated] = useState('');

	const {postId} = useParams();

	useEffect(() => {

		fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
		.then(res => res.json())
		.then(data => {

			setTitle(data.title);
			setBody(data.body);
		})
	}, []);

	return (
		<Container className="p-3">
			<Form>
			    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
			        <Form.Label>Title</Form.Label>
			        <Form.Control type="text" value={title} />
			    </Form.Group>
			    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
			        <Form.Label>Content</Form.Label>
			        <Form.Control as="textarea" rows={6} value={body}/>
			    </Form.Group>
			    <Button type="submit">Update</Button>
			    <Button as={Link} to={`/`} className="ms-3 btn-danger">Cancel</Button>
			</Form>
		</Container>
	)
}