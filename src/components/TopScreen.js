import {useEffect, useState} from 'react';
import {Container, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import ArticleCards from './ArticleCards';

export default function TopScreen(){

	const [article, setArticle] = useState('');

	useEffect(() => {
		fetch('https://jsonplaceholder.typicode.com/posts')
		.then(res => res.json())
		.then(data => {
			setArticle(data.slice(0, 3).map(post => {
				return(
					<ArticleCards key={post.id} articleProp={post}/>
				)
			}))
		})
	}, [])

	return (
		<Container className="p-3">
			<Button as={Link} to={`/new`}>Create New Article</Button>
			<Container>
				{article}
			</Container>
		</Container>
	)
}